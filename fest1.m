function fest
% (c) Mikael Kuisma mikael.j.kuisma@jyu.fi
%
% First draft of implementation of Ehrenfest dynamics in fictitious
% 1D-TDDFT. Right now, the propagator is trivial and error O(dt). 

totaltime = 8;
stdE = [];
dts = [];

for dt = logspace(log(0.1), log(0.001),10)
% 1. Ehfenfest example code: Grid wave function 1D-ehrenfest code
L_s = [ -40 0 ]; % Unit cell
Na = 3;
Nb = 3;
f=2.0;
Axc=1.5;
totalsteps = ceil(totaltime/dt);
Z_a = [ 1 3 2 ]; % Charge of the nuclei 
R_a = [ -22 -24 -10 ]; % Positions of nuclei
%v_a = [ 0 0 -0.64 ]; % Velocities of nuclei
v_a = [ 0 0 -2.30 ]; % Velocities of nuclei
M_a = [ 200 200 10];
traj_ai=zeros(Na,totalsteps);
energies = zeros(4, totalsteps);
energies(:) = NaN;
aext_a = [ 0 0 0]; % External acceleration on nuclei
g = 1200; % Number of grid points
L = L_s(2)-L_s(1); % Width of unit cell
h = L / g; % Grid spacing
x=(0:(g-1))'*h + L_s(1); % Positions of grid points

% Build 1D Laplacian stencil
e=ones(g,1);
T=-0.5*spdiags([e -2*e e], -1:1, g, g)/h^2;
V=spdiags([Vext(x, Z_a, R_a, 0)], 0, g,g);
I=speye(g);
[X1,X2] = meshgrid(x,x);
W = 1./sqrt( (X1-X2).^2 + 1.0) * h;

% SCF ground state iteration
nold_g=0;
clf
while 1
H = T+V;
[PSI,E] = eigs(H,Nb,'sa');
PSI = PSI / sqrt(h);

% Calculate density
   n_g = zeros(g,1);
   for n=1:Nb
       n_g = n_g + f*abs(PSI(:,n)).^2;
   end
   nold_g=nold_g*0.5+n_g*0.5;
   Vha = W*nold_g;
   Vxc = -Axc*4/3*(nold_g).^(1/3);

      % Calculate total energy
   % Electronic kinetic energy
   Eel = f*real(trace(PSI'*T*PSI))*h;
   
   % xc energy
   Eel = Eel -Axc*sum(n_g.^(4/3))*h;
   
   % Hartree energy
   Eel = Eel + 0.5 * n_g'*W*n_g*h;
   
   % Externa-l energy
   Eel = Eel + sum(Vext(x, Z_a, R_a, 0).*n_g)*h;
   
   % Nuclear kinetic energy
   EnT = 0.5*sum(M_a.*(v_a.^2));
   % Nuclear potential energy
   EnV = 0;
   for a1=1:Na
       for a2=a1+1:Na
         EnV = EnV + Z_a(a1)*Z_a(a2)*((R_a(a1)-R_a(a2))^2+1)^(-1/2);
       end
   end
   Etot = Eel+EnV+EnT;
   
   V=spdiags([Vext(x, Z_a, R_a, 0)+Vha+Vxc], 0, g,g);
   %plot(x,n_g);
      diff=sum(abs(nold_g-n_g))*h;
   disp(num2str(diff))
   if diff<1e-10
       break
   end
   %pause(0.001);
end
% Give a boost
for i=1:3
PSI(:,i) = PSI(:,i) .* exp(1j*v_a(3)*x.*((erf((x+16))+1)/2));
end


iteration=1;
t=0;
while 1
    if iteration>totalsteps
        break
    end
   Vha = W*n_g;
   Vxc = -Axc*4/3*n_g.^(1/3);
   V=spdiags([Vext(x, Z_a, R_a, 0)+Vha+Vxc], 0, g,g);
   H = T+V;
   
   % Calculate total energy
   % Electronic kinetic energy
   Eel = f*real(trace(PSI'*T*PSI))*h;
   
   % xc energy
   Eel = Eel -Axc*sum(n_g.^(4/3))*h;
   
   % Hartree energy
   Eel = Eel + 0.5 * n_g'*W*n_g*h;
   
   % External energy
   Eel = Eel + sum(Vext(x, Z_a, R_a, 0).*n_g)*h; 
   
   % Nuclear kinetic energy
   EnT = 0.5*sum(M_a.*(v_a.^2));
   % Nuclear potential energy
   EnV = 0;
   for a1=1:Na
       for a2=a1+1:Na
         EnV = EnV + Z_a(a1)*Z_a(a2)*((R_a(a1)-R_a(a2))^2+1)^(-1/2);
       end
   end
   energies(:,iteration) = [ Eel EnV EnT Eel+EnV+EnT];
   PSI = (I + 0.5j*dt*H) \ ((I - 0.5j*dt*H)*PSI);
   
   % Velocity verlet
   a_a = aext_a;
   
   % Nuclei-nuclei interaction
   for a1=1:Na
   for a2=1:Na
       if a1 ~= a2
           a_a(a1) = a_a(a1) + Z_a(a1)*Z_a(a2)*((R_a(a1)-R_a(a2))^2+1)^(-3/2)*(R_a(a1)-R_a(a2));
       end
   end
   end
   % Calculate density
   n_g = zeros(g,1);
   for n=1:3
       n_g = n_g + f*abs(PSI(:,n)).^2;
   end

   
   % Calcualte derivative of external potential
   dVdR_ga = Vext(x, Z_a, R_a, 1);
   % Calculate accelerations
   a_a = a_a - sum(dVdR_ga .* repmat(n_g, 1, Na),1)*h; 
   
   % Verlet propagation
   v_a = v_a + a_a./M_a * dt;
   R_a = R_a + v_a * dt;
   traj_ai(:,iteration) = R_a;
   if mod(iteration, 200000000) == 0 %<- never  
    %subplot(3,1,1);
     figure(1);
       hold off
     plot(x,diag(V),'k','Linewidth',2); % External potential
     hold on;
     plot(x,real(PSI(:,3)),'r'); % WF of HOMO
     plot(x,imag(PSI(:,3)),'b'); 
     plot(x,n_g,'k'); % Total density

   ylim([-6 6]);
   title([num2str(iteration) ' ' num2str(t)])
   if 1
   figure(2);
   hold off
   %subplot(3,1,2);
   plot(traj_ai');
   ylim([-40 00]);
   %subplot(3,1,3);
   figure(3);
   hold off
   plot(energies(end,:)');
   title('Total energy');
   pause(0.0001);
   end
   end
  t=t+dt;
  iteration = iteration + 1;
end

figure(4);
stdE = [stdE std(energies(end,:))];
dts = [ dts dt ];
loglog(dts, stdE,'x');
global stdE
global dts
xlabel('log timestep');
ylabel('log std E');
pause(1);
end

function V = Vext(x, Z_a, R_a, derivative)
if derivative == 0
    Na = length(Z_a);
    V = zeros(size(x));
    for a=1:Na
        V = V - Z_a(a) ./ sqrt((R_a(a) - x).^2 + 1);
    end
else 
    Na = length(Z_a);
    V = zeros(prod(size(x)), Na);
    for a=1:Na
        V(:,a) = gradient(Z_a(a) ./ sqrt((R_a(a) - x).^2 + 1),x(2)-x(1));
    end    
end
